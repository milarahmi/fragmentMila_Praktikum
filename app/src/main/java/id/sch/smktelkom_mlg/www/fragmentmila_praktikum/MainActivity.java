package id.sch.smktelkom_mlg.www.fragmentmila_praktikum;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void btnToast(View view) {
        Intent intent = new Intent(getApplicationContext(), ToastApp.class);
        startActivity(intent);
    }

    public void btnAlarm(View view) {
        Intent intent = new Intent(getApplicationContext(), AlarmApp.class);
        startActivity(intent);
    }

    public void btnMaps(View view) {
        Intent intent = new Intent(getApplicationContext(), MapsApp.class);
        startActivity(intent);
    }

    public void btnPicture(View view) {
        Intent intent = new Intent(getApplicationContext(), PictureApp.class);
        startActivity(intent);
    }
}